#pragma strict

var speed = 20;
var Laser : Transform;
var explosion : Transform;

var sound : AudioClip;
var sound1 : AudioClip;

private var spPoint : GameObject;

function Start () {
	spPoint = GameObject.Find("spawnPoint");
	EnterGunship();
}

function Update () {
	var amtToMove = speed * Time.deltaTime;
	var front = Input.GetAxis("Horizontal") * amtToMove;
	var up = Input.GetAxis("Vertical") * amtToMove;
	
	if(jsGameManager.state == STATE.IDLE){
		transform.Translate(Vector3(front, up, 0));
		
		transform.position.x = Mathf.Clamp(transform.position.x, -29, 29);
		transform.position.y = Mathf.Clamp(transform.position.y, -16, 17);
		
		if(Input.GetButtonDown("Fire1"))
		{
			Instantiate(Laser, spPoint.transform.position, Quaternion.identity);
			AudioSource.PlayClipAtPoint(sound, transform.position);
		}
	}
}

function OnTriggerEnter(coll : Collider)
{
	if(jsGameManager.state == STATE.ENTER || jsGameManager.state == STATE.WAIT
		|| coll.gameObject.tag != "ASTEROID"){
		
		return;
	}
	
	Instantiate(explosion, transform.position, Quaternion.identity);
	coll.SendMessage("SetAsteroid", SendMessageOptions.DontRequireReceiver);
	AudioSource.PlayClipAtPoint(sound1, transform.position);
	
	if(jsGameManager.undeadCnt <= 0){
		jsGameManager.state = STATE.DESTROY;
	}
	
}

function EnterGunship()
{
	jsGameManager.state = STATE.ENTER;
	transform.position = Vector3(-45, 0, 0);
	yield WaitForSeconds(1);
	
	var amtToMove = 10 * Time.deltaTime;
	while(transform.position.x < - 29){
		transform.Translate(Vector3.right * amtToMove);
		yield 0;
	}
	
	jsGameManager.undeadCnt = 3;
	Undead();
	jsGameManager.state = STATE.IDLE;
}

function Undead()
{
	var child = transform.Find("Space_Shooter");
	
	for(var i = 1; i<=jsGameManager.undeadCnt; i++){
		child.renderer.material.color = Color.red;
		yield WaitForSeconds(0.5);
		
		child.renderer.material.color = Color.white;
		yield WaitForSeconds(0.5);
	}
	
	jsGameManager.undeadCnt = 0;
}