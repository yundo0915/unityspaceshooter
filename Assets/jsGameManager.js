#pragma strict

var gunship : Transform;
var asteroid : Transform;
var bonus : Transform;
var explosion : Transform;
var sound : AudioClip;

var txtScore1 : GUIText;
var txtScore2 : GUIText;
var txtScore3 : GUIText;
var txtScore4 : GUIText;
var txtScore5 : GUIText;
var txtMsg : TextMesh;

private var shipCnt = 5;
private var asterCnt = 0;
private var score = 0;
private var bonusCnt = 0;
private var stageNum = 1;

static var bonusNum;
private var isAsteroid = true;

enum STATE{START, CLEAR, OVER, HIT, BONUS, DESTROY, IDLE, ENTER, WAIT};
static var state = STATE.START;

static var transPos : Vector3;
static var undeadCnt : int;

function Start () {

}

function Update () {
	switch(state){
		case STATE.START :
			StageStart();
			break;
		case STATE.CLEAR :
			StageClear();
			break;
		case STATE.OVER :
			GameOver();
			break;
		case STATE.HIT :
			AsteroidHit();
			break;
		case STATE.BONUS :
			ProcessBonus();
			break;
		case STATE.DESTROY :
			GunshipDestroy();
			break;
	}
	
	Debug.Log(state);
}

function StageStart()
{
	var n = stageNum % 4;
	if(n==0)n = 4;
	
	var obj1 = GameObject.Find("back");
	var obj2 = GameObject.Find("back2");
	
	obj1.renderer.material.mainTexture = Resources.Load("sky" + n, Texture2D);
	obj2.renderer.material.mainTexture = Resources.Load("sky" + n, Texture2D);
	
	gunship.SendMessage("EnterGunship", SendMessageOptions.DontRequireReceiver);
	yield WaitForSeconds(3);
	
	for(var i = 1; i<=6 + stageNum; i++)
	{
		Instantiate(asteroid, Vector3(40, 0,0), Quaternion.identity);
	}
	state = STATE.IDLE;
}

function StageClear()
{
	state = STATE.WAIT;
	
	stageNum++;
	isAsteroid = false;
	ClearAsteroid();
	BlinkMsg(1);
	
	var speed = 20;
	while (gunship.transform.position.x < 50){
		var amtToMove = speed * Time.deltaTime;
		gunship.transform.Translate(Vector3.right * amtToMove);
		yield 0;
	}
	
	yield WaitForSeconds(1);
	
	isAsteroid = true;
	state = STATE.START;
}

function GameOver()
{
	state = STATE.WAIT;
	
	var exp = Instantiate(explosion, gunship.transform.position, Quaternion.identity);
	gunship.transform.position.x = -50;
	exp.particleEmitter.minEnergy=1.5;
	exp.particleEmitter.maxEnergy=1.5;
	
	BlinkMsg(2);
	yield WaitForSeconds(4);
	
	Application.LoadLevel("StartGame01");
}

function AsteroidHit()
{
	if(state == STATE.WAIT) return;
	
	state = STATE.IDLE;
	score += 10;
	asterCnt++;
	var rnd = Random.Range(0, 100);
	
	if(rnd >= 80){
		MakeBonus();
	}
	
	if(score>=stageNum * 500){
		state = STATE.CLEAR;
	}
}

function ClearAsteroid()
{
	for(var obj : GameObject in GameObject.FindGameObjectsWithTag("ASTEROID")){
		if(obj == null) continue;
		Instantiate(explosion, obj.transform.position, Quaternion.identity);
		AudioSource.PlayClipAtPoint(sound, obj.transform.position);
		
		if(isAsteroid == true){
			obj.SendMessage("SetAsteroid", SendMessageOptions.DontRequireReceiver);
			score += 10;
			asterCnt ++;
		}
		else
		{
			Destroy(obj);
		}
		yield WaitForSeconds(0.2);
	}
}

function BlinkMsg(num : int)
{
	if(num == 1){
		txtMsg.text = "Stage Clear!";
	}else{
		txtMsg.text = "You Lose Game!";
	}
	
	for( var i = 1; i<5; i++){
		txtMsg.renderer.enabled = true;
		yield WaitForSeconds(0.5);
		
		txtMsg.renderer.enabled = false;
		yield WaitForSeconds(0.5);
	}
}

function ProcessBonus()
{
	bonusCnt++;
	switch(bonusNum){
		case "0" :
			shipCnt++;
			break;
		case "1" :
			undeadCnt++;
			break;
		case "2" :
			ClearAsteroid();
			break;
			
	}
	
	if(state != STATE.WAIT){
		state = STATE.IDLE;
	}
}

function GunshipDestroy()
{
	shipCnt--;
	
	if(shipCnt >0)
	{
		gunship.SendMessage("EnterGunship", SendMessageOptions.DontRequireReceiver);
		
	}
	else
	{
		state = STATE.OVER;
	}
	
}

function MakeBonus()
{
	var Colors = new Array(Color.green, Color.blue, Color.red);
	var n = Random.Range(0, 3);
	
	var obj = Instantiate(bonus, transPos, Quaternion.identity);
	obj.Find("Sphere").renderer.material.color = Colors[n];
	obj.tag = "BONUS" + n;
}

function OnGUI()
{
	txtScore1.text = "Stage : " + stageNum;
	txtScore2.text = "Ship  : " + shipCnt;
	txtScore3.text = "Score : " + score;
	txtScore4.text = "Aster : " + asterCnt;
	txtScore5.text = "Bonus : " + bonusCnt;
}

