#pragma strict

var speed = 50;
var explosion : Transform;
var sound  : AudioClip;
var sound1 : AudioClip;

function Start () {

}

function Update () {
	var amtToMove = speed * Time.deltaTime;
	transform.Translate(Vector3.right * amtToMove);
	
	if(transform.position.x > 37)
		Destroy(gameObject);
}

function OnTriggerEnter(coll : Collider){
	if(jsGameManager.state == STATE.WAIT || jsGameManager.state == STATE.ENTER)
		return;
		
	Destroy(gameObject);
	
	if(coll.gameObject.tag == "ASTEROID"){
		Instantiate(explosion, transform.position, Quaternion.identity);
		AudioSource.PlayClipAtPoint(sound, transform.position);
		
		jsGameManager.state = STATE.HIT;
		jsGameManager.transPos = coll.transform.position;
		
		coll.SendMessage("SetAsteroid", SendMessageOptions.DontRequireReceiver);
	}
	
	if(coll.gameObject.tag.Substring(0, 5) == "BONUS"){
		AudioSource.PlayClipAtPoint(sound1, coll.transform.position);
		jsGameManager.bonusNum = coll.gameObject.tag.Substring(5, 1);
		jsGameManager.state = STATE.BONUS;
		Destroy(coll.gameObject);
	}
	
}
